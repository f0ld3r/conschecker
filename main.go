package main

import (
	"./wlogs"
	"fmt"
)

func main() {
	api := wlogs.NewClassic("b00ff9ba127fc06edd7608009d7f75e0")
	reports, err := api.ReportsForGuild("Горизон Событий", "пламегор", "EU")
	if err != nil {
		panic(err)
	}

	for _, report := range reports {
		fmt.Println(report)
	}
}
