package wlogs

import "fmt"

type Report struct {
	Id        string
	Title     string
	Owner     string
	Zone      int64
	StartTime *int64 `json:"start"`
	EndTime   *int64 `json:"end"`
}

func (w *WarcraftLogs) ReportsForGuild(guildName, realm, region string) ([]*Report, error) {

	url := fmt.Sprintf("reports/guild/%s/%s/%s", guildName, realm, region)

	return w.reportsFromUrl(url)
}

func (w *WarcraftLogs) reportsFromUrl(url string) ([]*Report, error) {

	reports := []*Report{}
	err := w.Get(url, &reports)

	return reports, err
}
